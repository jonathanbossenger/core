<?php

namespace Skyrkt\Bentobox\Tests\Controllers;

use Skyrkt\Bentobox\Controllers\ModelController;

class ModelControllerTest extends \PHPUnit_Framework_TestCase {

	/** @test */
	public function it_should_print_hello_on_index()
	{
		$modelController = new ModelController();

		$this->assertEquals($modelController->index(), 'Hello!');
	}

}
