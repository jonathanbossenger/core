export class Header {

  constructor(headerElement, sidebarElement) {
    this.headerElement = headerElement;
    this.sidebarElement = sidebarElement;

    this.checkWindowWidth();
    this.bindBurgerListener();

    let timeOut;
    window.addEventListener('resize',
      function() {
        clearTimeout(timeOut);
        timeOut = setTimeout(function(){
          if (window.innerWidth > 768) {
            sidebarElement.classList.add('open');
            document.querySelector('body').classList.add('sidebar-open');
          }
        }, 100);
      });
  }

  checkWindowWidth() {
    if (window.innerWidth > 768) {
      this.toggleSidebarOpen();
    }
  }

  bindBurgerListener() {
    this.headerElement.querySelector('.burger').addEventListener('click', () => {
      this.toggleSidebarOpen();
    });
  }

  toggleSidebarOpen() {
    this.sidebarElement.classList.toggle('open');
  }


}
