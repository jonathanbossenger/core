export class Validator {
  validate(element, input) {
    let errors = [];

    errors = this.checkRequired(errors, element, input);
    errors = this.checkLength(errors, element, input);
    errors = this.checkPattern(errors, element, input);

    return errors;
  }

  checkRequired(errors, element, input) {
    if (element.attributes.required && !input.value) {
      errors.push(`${input.name} is required`);
    }
    return errors;
  }

  checkLength(errors, element, input) {
    const min = element.attributes.min;
    const max = element.attributes.max;

    if (min && input.value.length > 0 && input.value.length < min.value) {
      errors.push(`Must be more than ${min.value} characters`);
    }
    if (max && input.value.length > 0 && input.value.length > max.value) {
      errors.push(`Must be less than ${max.value} characters`);
    }
    return errors;
  }

  checkPattern(errors, element, input) {
    if (
      element.attributes.pattern &&
      input.value &&
      input.value.match(element.attributes.pattern.value)
    ) {
      errors.push(element.attributes.message.value)
    }
    return errors;
  }
}
