<div class="form-group material">
		<label for="{{$attribute_name}}">@bentoLabel($attribute)</label>
		<input type="password" @bentoInput($attribute)>
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>
<div class="form-group material>
		<label for="{{$attribute_name}}_confirmation">{{array_get($attribute, 'label')}} Confirmation {{ array_get($attribute, 'required') ? '*' : '' }}</label>
		<input id="{{$attribute_name}}_confirmation" name="{{$attribute_name}}_confirmation" value="" type="password" {{ array_get($attribute, "required") ? "required" : "" }}>
</div>
