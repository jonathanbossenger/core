<div class="form-group material-wysiwyg">
	<label for="{{$attribute_name}}">@bentoLabel($attribute)</label>

	<textarea id="{{$attribute_name}}"
						class="editable"
						name="{{$attribute_name}}"
						{{ array_get($attribute, "required") ? "required" : "" }}>
						{{$model->$attribute_name}}
	</textarea>

	@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>

<script>
	tinymce.init({
		selector:'textarea.editable',
		plugins: 'link image code',
		menubar: 'insert',
		toolbar: 'undo redo | styleselect | bold italic | bullist numlist | alignleft aligncenter alignright alignjustify | link image | code',
		content_css: '/vendor/bentobox/wysiwyg-content.css',
		menubar: false,
		init_instance_callback: function (editor) {
			editor.on('focus', function (e) {
				this.editorContainer.parentElement.classList.add('is-focussed');
			});
			editor.on('blur', function (e) {
				this.editorContainer.parentElement.classList.remove('is-focussed');
			});
		},
		// enable title field in the Image dialog
		image_title: true,
		// enable automatic uploads of images represented by blob or data URIs
		automatic_uploads: true,
		// here we add custom filepicker only to Image dialog
		file_picker_types: 'image',
		images_upload_handler: function (blobInfo, success, failure) {
			var xhr, formData;

			xhr = new XMLHttpRequest();
			xhr.withCredentials = false;
			xhr.open('POST', '/{{config('bentobox.route_prefix', 'admin')}}/{{ array_get($modelConfig, 'url') }}/image');
			xhr.setRequestHeader('X-CSRF-TOKEN', '{{ csrf_token() }}');

			xhr.onload = function() {
				if (xhr.status != 200) {
					failure('HTTP Error: ' + xhr.status);
					return;
				}

				success(xhr.responseText);
			};

			formData = new FormData();
			formData.append('file', blobInfo.blob(), blobInfo.filename());

			xhr.send(formData);
		},
		// and here's our custom image picker
		file_picker_callback: function(cb, value, meta) {
			var input = document.createElement('input');
			input.setAttribute('type', 'file');
			input.setAttribute('accept', 'image/*');

			// Note: In modern browsers input[type="file"] is functional without
			// even adding it to the DOM, but that might not be the case in some older
			// or quirky browsers like IE, so you might want to add it to the DOM
			// just in case, and visually hide it. And do not forget do remove it
			// once you do not need it anymore.

			input.onchange = function() {
				var file = this.files[0];

                            var reader = new FileReader();
                            reader.readAsDataURL(file);

                            reader.onload = function () {
                                // Note: Now we need to register the blob in TinyMCEs image blob
                                // registry. In the next release this part hopefully won't be
                                // necessary, as we are looking to handle it internally.
                                var id        = 'blobid' + (new Date()).getTime();
                                var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                                var base64    = reader.result.split(',')[1];
                                var blobInfo  = blobCache.create(id, file, base64);
                                blobCache.add(blobInfo);

                                // call the callback and populate the Title field with the file name
                                cb(blobInfo.blobUri(), {title: file.name});
                            };
			};

			input.click();
		}
	});
</script>
