<?php
	if ($model->id) {
		$value = $model->bentobox_media()->media([$attribute_name, 'bentobox_thumb']);
	}
?>

{{-- FILE UPLOAD --}}
<div class="form-group file-input">
	<div class="preview"
		@if (isset($value))
			style="display: block"
		@endif
	>
		@if(isset($value) && isset($value->disk) && isset($value->url))
			<img src="{{ Storage::disk($value->disk)->url($value->url) }}">
		@endif
	</div>

	<div class="input">
		<label for="{{ $attribute_name }}">@bentoLabel($attribute)</label>
		<input type="file" name="{{ $attribute_name }}">
		<button type="button" class="secondary-button">Choose File</button>
		<div class="message">
			@if (isset($value) && isset($value->disk) && isset($value->url))
				{{ $value->url }}
			@else
				(No file chosen)
			@endif
		</div>
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
	</div>
</div>

{{-- TITLE FIELD --}}
<div class="form-group material">
	<label for="bentobox_{{ $attribute_name }}_title">{{array_get($attribute, 'label')}} Title {{ array_get($attribute, 'required') ? '*' : '' }}</label>
	<input type="text"
				 name="bentobox_{{ $attribute_name }}_title"
				 id="bentobox_{{ $attribute_name }}_title"

				 @if(isset($value->title))
					 value="{{ $value->title }}"
				 @endif

				 {{ array_get($attribute, "required") ? "required" : "" }}>
</div>

{{-- ALT TEXT FIELD --}}
<div class="form-group material">
	<label for="bentobox_{{ $attribute_name }}_alt">{{array_get($attribute, 'label')}} Alt Text {{ array_get($attribute, 'required') ? '*' : '' }}</label>
	<input type="text"
				 name="bentobox_{{ $attribute_name }}_alt"
				 id="bentobox_{{ $attribute_name }}_alt"

				 @if(isset($value->alt))
					 value="{{ $value->alt }}"
				 @endif

				 {{ array_get($attribute, "required") ? "required" : "" }}>
</div>
