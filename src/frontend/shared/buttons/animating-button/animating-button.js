export class AnimatingButton {
  constructor(element) {
    this.element = element;
    this.canAnimate = true;

    this.player = this.createAnimation(this.element);
    this.checkAnimation(this.element);
    this.bindScrollListener(this.element);
  }

  checkAnimation(element) {
     if(window.scrollY > 0 && this.canAnimate && window.innerWidth >= 750){
      this.play(this.player); 
      element.classList.add('fab-state');
      this.canAnimate = false;
     } else if (!this.canAnimate && window.scrollY === 0 && window.innerWidth >= 750) {
      this.reverse(this.player); 
      element.classList.remove('fab-state');
      this.canAnimate = true;
     }
  }

  bindScrollListener(element) {
    window.addEventListener('scroll', () => {
      this.checkAnimation(element);
    });
  }

  createAnimation(element) {
    const player = element.animate([
      {
        width: element.clientWidth + 'px',
        borderRadius: '0.3rem',
        transform: 'translateX(0)'
      },
      {
        width: '5rem',
        borderRadius: '50%',
        transform: 'translateX(50%)'
      },
    ], {
      duration: 300, //milliseconds
      easing: 'ease-in-out', //'linear', a bezier curve, etc.
      iterations: 1, //or a number
      fill: 'forwards'
    });
    player.pause();
    return player;
  }

  play(player) {
    player.playbackRate = 1;
    player.play();
  }

  reverse(player) {
    player.playbackRate = -1;
    player.play();
  }
}
