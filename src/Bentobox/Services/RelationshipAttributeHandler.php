<?php

namespace Skyrkt\Bentobox\Services;

class RelationshipAttributeHandler
{

	/**
	 * Constant prefix for relationship attributes
	 * @var string
	 */
	private static $RELATIONSHIP_PREFIX = 'bentobox_relationship_';

	/**
	 * Looks through the full request for any attribute that matches the relationship prefix
	 * if that's the case, then deal with it accordingly
	 * @param  Model $model
	 * @param  Request $request
	 * @param  Array $modelAttributeConfig  list of configuration options for the model
	 * @return Array                      A list of relationships parsed out of the request
	 */
	public static function handleRelationships($model, $request, $modelAttributeConfig)
	{
		$request = collect($request);

		$relationshipAttributes = $request->filter(function($item, $name) {
			return str_contains($name, self::$RELATIONSHIP_PREFIX);
		});

		$relationshipAttributes = $relationshipAttributes->mapWithKeys(function($attribute) {
			$exploded = explode("_", $attribute);
			return [$exploded[0] => $exploded[1]];
		});

		self::saveRelationships($model, $request, $modelAttributeConfig, $relationshipAttributes);

		return $relationshipAttributes;
	}

	/**
	 * Look through the relationships recieved and save them accordingly.
	 * @param  Model $model
	 * @param  Request $request
	 * @param  Array $modelAttributeConfig  The configuration of the model passed
	 * @param  Array $relationshipAttributes  Parsed array of request relationships
	 */
	private static function saveRelationships($model, $request, $modelAttributeConfig, $relationshipAttributes)
	{
		$relationshipAttributes->map(function($method, $relationship) use ($model, $request, $modelAttributeConfig) {
			$config = $modelAttributeConfig[$relationship];
			self::$method($model, $request, $relationship, $config);
		});
	}

	/**
	 * Handle saving of a morphToMany relationship.
	 * @param  Model $model
	 * @param  Request $request
	 * @param  string $relationship Relationship name
	 * @param  Array $config        configuration for the attribute that this relationship represents
	 */
	private static function morphToMany($model, $request, $relationship, $config)
	{
		$toSync = ($request->has($relationship)) ? $request[$relationship] : [];
		$model->$relationship()->sync($toSync);
	}

}
