<?php

namespace Skyrkt\Bentobox\Services\Inferrers;

use Skyrkt\Bentobox\Helpers\StringHelper;

class ConfigurationInferrer {

	/**
	 * Get configuration from off of model. If the model has bentobox_attributes specified, we'll
	 * use that array to determine what to return. Otherwise we fallback to the fillable array
	 * for values
	 *
	 * @param  Model $model
	 * @return array
	 */
	public function makeConfigFromModel($model)
	{
		$customConfiguration = ($model->bentobox_attributes) ?
			$this->makeConfigFromAttributeArray($model->bentobox_attributes) : [];

		$inferredConfiguration = (!config('bentobox.manual_inclusion_mode') && $model->getFillable()) ?
			$this->makeConfigFromAttributeArray($model->getFillable()) : [];

		return array_merge($inferredConfiguration, $customConfiguration);
	}

	/**
	 * Takes an array of attributes and optionally the values and returns
	 * an array that bentobox attributes can recognize
	 *
	 * @param  array $rawConfig
	 * @return array
	 */
	public function makeConfigFromAttributeArray($rawConfig)
	{
		$parsedConfig = [];

		foreach ($rawConfig as $label => $value) {
			$parsedConfig = array_merge($parsedConfig,
				$this->parseRawConfigItem($label, $value));
		}

		return $parsedConfig;
	}

	/**
	 * Build configuration and use defaults if for settings not present.
	 *
	 * @param  string $label
	 * @param  string $value
	 * @return array
	 */
	private function parseRawConfigItem($label, $value)
	{
		if (is_int($label)) {
			return [$value => $this->defaultConfiguration($value)];

		} else {
			return [
				$label => array_merge($this->defaultConfiguration($label),
					$this->parseShorthandOptions($value))
			];
		}

		return [$label => $value];
	}

	/**
	 * Compare against default configuration array and overwrite any
	 * custom settings.
	 *
	 * @param  string $rawLabel
	 * @return array
	 */
	private function defaultConfiguration($rawLabel)
	{
		return [
			'type' => 'text',
			'label' => StringHelper::humanize($rawLabel),
			'index' => true,
			'edit' => true,
			'required' => false
		];
	}

	/**
	 * If the shorthand options are provided for a configuration item,
	 * parse these into a proper array format
	 *
	 * @param  string|array $options
	 * @return array
	 */
	private function parseShorthandOptions($options)
	{
		if (is_array($options)) {
			return $options;

		} else {
			$parsedOptions = [];
			foreach (explode('|', $options) as $option) {
				$splitOption = explode(':', $option);
				$parseOptions[$splitOption[0]] = isset($splitOption[1]) ? $splitOption[1] : true;
			}

			return $parseOptions;
		}
	}
}
