<?php

namespace Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\Version;

use App;
use Skyrkt\Bentobox\Services\SimpleFileUpload;
use Skyrkt\Bentobox\Services\RelationshipAttributeHandler;
use Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\ModelViewType;

/**
 * Methods for the CRUD operations that are associate with the "Single" model type. Keep in mind there
 * is no index on the single type, just a show. But because we extend model type we need to implement it.
 */
class Version extends ModelViewType {

	/**
	 * Just serve up the latest record found of this model
	 * @param  Request $request   the request made to the server
	 * @return Blade | JSON       a view or JSON array of records
	 */
	public function index($request)
	{
		return $this->show($request, 0);
	}

	/**
	 * Just serve up the latest record found of this model
	 * @param  Request $request    The request made
	 * @param  number | string $id Identifier of the specific class
	 * @return Blade | Json        The way we want to return this
	 */
	public function show($request, $id)
	{
		$model = $this->modelClass::latest()->first();

		if (!$model) {
			$model = new $this->modelClass();
		}

		$id = $model->id;

		$attributes = collect($this->modelAttributeConfig)->filter(function($attribute) {
		$edit = array_get($attribute, "edit", true);
		if (is_string($edit)) { $edit = ($edit === 'true'); }
		return $edit;
		});

		return view('bentobox::model-view-types.version.version-item')
			->with('modelConfig', $this->modelConfig)
			->with('model', $model)
			->with('attributes', $attributes);
	}

	/**
	 * Store the version
	 * @param  Request $request  the form elements in the Request
	 * @param  number $id        id of the collection if editing
	 * @return redirect          redirecting if success, errors if not!
	 */
	public function store($request, $id)
	{
		if ($id) {
			$model = $this->modelClass::findOrFail($id);
		} else {
			$model = new $this->modelClass;
		}

		SimpleFileUpload::upload($request, $this->modelAttributeConfig);

		$model->fill($request->all());

		if ($model->save()) {

			// We have special methods that handle parsing and saving of the relationships.
			// We should really consider making this a response to model save being fired.
		RelationshipAttributeHandler::handleRelationships($model, $request, $this->modelAttributeConfig);

		return redirect(('/'. config('bentobox.route_prefix', 'admin') . '/' . array_get($this->modelConfig, 'url')));

		} else {

		return back()->withErrors($model->bento_errors)->withInput();

		}
	}

	/**
	 * Delete the record
	 * @param  Request $request the request of the call
	 * @param  number $id id of the record
	 * @return redirect     redirect to view
	 */
	public function delete($request, $id)
	{
		$this->modelClass::findOrFail($id)->delete();

		return redirect(('/'. config('bentobox.route_prefix', 'admin') . '/' . array_get($this->modelConfig, 'url')));
	}
}
