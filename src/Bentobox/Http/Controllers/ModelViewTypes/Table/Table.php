<?php

namespace Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\Table;

use App;
use Route;
use Redirect;
use Skyrkt\Bentobox\Services\SimpleFileUpload;
use Skyrkt\Bentobox\Services\RelationshipAttributeHandler;
use Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\ModelViewType;
use Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\Table\TableIndex;

class Table extends ModelViewType {

	/**
	* Get a list of records
	* @param  Request $request   the request made to the server
	* @return Blade | JSON       a view or JSON array of records
	*/
	public function index($request)
	{
		// TODO: Move this out to a dedicated route so that any controller
		// can access the sorting feature via async or regular request
		if ($request->usort == true) {
			$attr = $request->attribute;

			// Check if the model has un-indexed records and reset if it does.
			$unindexedRecords = $this->modelClass::where($attr, 0)->get();
			if (count($unindexedRecords) > 0) {
				$allRecords = $this->modelClass::all();

				$value = 1;
				foreach ($allRecords as $record)
				{
					$record[$attr] = $value;
					$record->save();
					$value = $value + 1;
				}

				return redirect()->back();
			}

			$targetModel = $this->modelClass::findOrFail($request->id);

			if ($request->increment == 'up') {
				$nextModel = $this->modelClass::where($attr, '>', $targetModel[$attr])->orderBy($attr, 'asc')->first();

			} elseif ($request->increment == 'down') {
				$nextModel = $this->modelClass::where($attr, '<', $targetModel[$attr])->orderBy($attr, 'desc')->first();
			}

			if ($nextModel) {
				$targetOld = $targetModel[$attr];
				$nextOld = $nextModel[$attr];
				$targetModel[$attr] = $nextOld;
				$nextModel[$attr] = $targetOld;
				$targetModel->save();
				$nextModel->save();

				return redirect()->back();

			} else {
				return redirect()->back();
			}
		}

		$index_service = App::make(TableIndex::class);

		// Collect and Filter out all the attributes that are not shown in Index
		$filteredAttributes = collect($this->modelAttributeConfig)->filter(function($attribute) {
			$index = array_get($attribute, "index", true);
			if (is_string($index)) { $index = ($index === 'true'); }
			return $index;
		});


		$order = $this->getOrderBy($request);
		$model = $this->modelClass::orderBy($order['by'], $order['direction'])
			->paginate($this->modelConfig->get('perPage'));

		// Put the edit attribute on the attributes, so it has an edit button
		// $filteredAttributes->put('edit', [ 'label' => 'Edit', 'type' => 'edit' ]);

		// Send the records, attributes and config to a mutator that makes the data
		// look nice.
		$records = $index_service->mutateEntries(
			$model,
			$filteredAttributes,
			$this->modelConfig,
			$this->modelAttributeConfig
		);

		return view('bentobox::model-view-types.table.main.table-index')
			->with('modelConfig', $this->modelConfig)
			->with('records', $records)
			->with('attributes', $filteredAttributes)
			->with('order', $order)
			->with('pagination', $model->appends(
					[
						'orderBy' => $request->input('orderBy'),
						'orderDirection' => $request->input('orderDirection')
					])
				->links('bentobox::model-view-types.table.main.pagination._pagination'));
	}

	/**
	* Display the table item.
	* @param  Request $request    The request made
	* @param  number | string $id Identifier of the specific class
	* @return Blade | Json        The way we want to return this
	*/
	public function show($request, $id)
	{
		if ($id) {
			$model = $this->modelClass::findOrFail($id);
		} else {
			$model = new $this->modelClass();
		}

		$attributes = collect($this->modelAttributeConfig)->filter(function($attribute) {
			$edit = array_get($attribute, "edit", true);
			if (is_string($edit)) { $edit = ($edit === 'true'); }
			return $edit;
		});

		// Check if JSON is wanted and return the model if it is
		if ($request->wantsJson()) {
			return response()->json($model);
		}

		return view('bentobox::model-view-types.table.item.table-item')
			->with('modelConfig', $this->modelConfig)
			->with('model', $model)
			->with('attributes', $attributes);
	}

	/**
	* Store the collection
	* @param  Request $request  the form elements in the Request
	* @param  number $id        id of the collection if editing
	* @return redirect          redirecting if success, errors if not!
	*/
	public function store($request, $id)
	{
		if ($id) {
			$model = $this->modelClass::findOrFail($id);
		} else {
			$model = new $this->modelClass;
		}

		SimpleFileUpload::upload($request, $this->modelAttributeConfig);

		$model->fill($request->all());

		if ($model->save()) {
			// We have special methods that handle parsing and saving of the relationships.
			// We should really consider making this a response to model save being fired.
			RelationshipAttributeHandler::handleRelationships($model, $request, $this->modelAttributeConfig);

			return redirect(('/'. config('bentobox.route_prefix', 'admin') . '/' . array_get($this->modelConfig, 'url')));

		} else {

			return back()->withErrors($model->bento_errors)->withInput();

		}
	}

	/**
	* Delete the record
	* @param  number $id id of the record
	* @return redirect     redirect to correct view
	*/
	public function delete($request, $id)
	{
		if (isset($id)) {
			$this->modelClass::findOrFail($id)->delete();
		} else {
			$ids = explode(',', $request->ids_to_delete);

			$this->modelClass::destroy($ids);
		}

		return redirect(('/'. config('bentobox.route_prefix', 'admin') . '/' . array_get($this->modelConfig, 'url')));
	}

	/**
	 * Create the order by array to be used by the index
	 * @param  Request $request the request
	 * @return array          the sort instructions
	 */
	private function getOrderBy($request)
	{
		$by = $request->query('orderBy', 'created_at');
		$direction = $request->query('orderDirection', 'DESC');
		return [
			'by' => $by,
			'direction' => $direction
		];
	}
}
