<?php

namespace Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\Table;

class TableIndex {

	protected $modelConfig;
	protected $modelAttributeConfig;

	public function mutateEntries($records, $attributes, $modelConfig, $modelAttributeConfig)
	{
		$this->modelConfig = $modelConfig;
		$this->modelAttributeConfig = $modelAttributeConfig;

		$mutatedEntries = $records->map(function($record, $name) use ($attributes) {
			return $this->mutateEntry($record, $name, $attributes);
		});

		return $mutatedEntries;
	}

	public function mutateEntry($record, $name, $attributes)
	{
		$newEntry = [];
		$first = true;

		foreach ($attributes as $attributeName => $attributeValue) {
			if ($first)
			{
				$newEntry[$attributeName] = "<a href=\"/" . config("bentobox.route_prefix", 'admin') . "/{$this->modelConfig['url']}/{$record['id']}\">".$record->$attributeName."</a>";
				$first = false;

			} else {
				switch (array_get($attributeValue, 'type')) {
					case 'date':
						if ($record->$attributeName) {
							$newEntry[$attributeName] = $this->mutateDate($record->$attributeName);
						}
						break;

					case 'checkbox':
						$newEntry[$attributeName] = $this->mutateCheckbox($record->$attributeName);
						break;

					case 'edit':
						$newEntry[$attributeName] = "<a href=\"/" . config("bentobox.route_prefix", 'admin') . "/{$this->modelConfig['url']}/{$record['id']}\">Edit</a>";
						break;

					case 'select':
						$newEntry[$attributeName] = $this->mutateSelect($record, $attributeName);
						break;

					case 'hasMany':
						$newEntry[$attributeName] = $this->mutateHasMany($record, $attributeName);
						break;

					case 'usort':
						$newEntry[$attributeName] = $this->mutateUsort($record, $attributeName);
						break;

					default:
						$newEntry[$attributeName] = $record->$attributeName;
						break;
				}
			}

		}

		$newEntry = collect($newEntry)->prepend($record['id'], 'id');

		return $newEntry;
	}

	private function mutateDate($date)
	{
		return $date->diffForHumans();
	}

	private function mutateCheckbox($boolean)
	{
		return ($boolean === 1 || $boolean === true) ? 'Yes' : 'No';
	}

	private function mutateSelect($record, $attributeName)
	{
		if (
			key_exists(
				$record[$attributeName],
				$this->modelAttributeConfig[$attributeName]['options']
			)
		) {
			return $this->modelAttributeConfig[$attributeName]['options'][$record[$attributeName]];
		} else {
			return $record[$attributeName];
		}
	}

	private function mutateHasMany($record, $attributeName)
	{
		$html = "";
		foreach ($record->$attributeName->all() as $item) {
			$attributeToGet = $this->modelAttributeConfig[$attributeName]['attribute'];
			$html = $html . $item->$attributeToGet . ', ';
		}
		return $html;
	}

	/**
	 * Fetches a usort attribute type and generates the appropriate links
	 * for user sorting functionality in table view
	 *
	 * @param  collection $record
	 * @param  string $attributeName
	 * @return string
	 */
	private function mutateUsort($record, $attributeName)
	{
		$increaseHtml = "<small><a href=\"/" . config("bentobox.route_prefix", 'admin') . "/{$this->modelConfig['url']}?usort=true&attribute={$attributeName}&increment=up&id={$record['id']}/\">+</a></small>";
		$decreaseHtml = "<small><a href=\"/" . config("bentobox.route_prefix", 'admin') . "/{$this->modelConfig['url']}?usort=true&attribute={$attributeName}&increment=down&id={$record['id']}/\">-</a></small>";
		return $record[$attributeName] . ' ' . $increaseHtml . ' ' . $decreaseHtml;
	}
}
