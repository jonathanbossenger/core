<?php

namespace Skyrkt\Bentobox\Models\Traits;

use Storage;

trait BentoboxMedia
{
	/**
	* Allow event listeners to check if the model is media enabled
	* @var boolean
	*/
	public $bentobox_media_enabled = true;

	/**
	* ELoquent relationship to Bentobox Media model
	* @return \Illuminate\Database\Eloquent\Relations\MorphMany
	*/
	public function bentobox_media()
	{
		return $this->morphMany('Skyrkt\Bentobox\Models\BentoboxMedia', 'model');
	}

	/**
	* Eloquent Query scope for fetching specific Media records from
	* the media model
	*
	* @param  array $scope
	* @return object
	*/
	public function media($scope)
	{
		return $this->bentobox_media()->media($scope);
	}

	/**
	* Accessor for getting the absolute path to the media
	* asset.
	*
	* @param  array $scope
	* @return object
	*/
	public function path($scope)
	{
		$media = $this->media($scope);

		if (config('bentobox.bentobox_media_cdn_url') && isset($media->url)) {
			return config('bentobox.bentobox_media_cdn_url') . '/' . $media->url;
		} elseif (isset($media->disk) && isset($media->url)) {
			return Storage::disk($media->disk)->url($media->url);
		} else {
			return null;
		}
	}
}
