<?php

namespace Skyrkt\Bentobox\Models;

use Illuminate\Database\Eloquent\Model;

abstract class BentoboxModel extends Model {

	/**
	 * An Array containing the configuration for specific attributes on the model
	 * in array syntax.
	 *
	 * ### Example Configuration
	 * ```php
	 * public $bentobox_config = [
	 *   'toggle' => true,
	 *   'disabled' => true,
	 *   'enabled' => true,
	 *   'request' => 'App\Request\CustomRequest',
	 *   'label' => 'Cool Articles',
	 *   'type' => 'Table',
	 *   'section' => 'Blog',
	 *   'perPage' => 50,
	 *   'url' => 'customUrl',
	 *   'urlParams' => [
	 *   	'page' => 1
	 *   ]
	 * ]
	 * ```
	 *
	 * @var array
	*/
	public $bentobox_config = [];

	/**
	 * An Array containing the configuration for specific attributes on the model
	 * in the default laravel syntax OR as an array.
	 *
	 * Each key of the index should be the name of a attribute on your model
	 *
	 * ### Example Configuration
	 *
	 * ```php
	 * // Laravel syntax
	 * public $bentobox_attributes = [
	 *     'body' => 'type:wysiwyg',
	 *     'published_at' => 'type:date|index:false|edit:true|panel:Dates|label:Cool Date!',
	 * ];
	 *
	 * // Array Syntax
	 * public $bentobox_attributes = [
	 *     'body' => [
	 *         'type' => 'wysiwyg'
	 *     ],
	 *     'published_at' => [
	 *         "type" => "date",
	 *         "index" => false,
	 *         "edit" => true,
	 *         "panel" => "Dates",
	 *         "label" => "Cool Date!"
	 *     ]
	 * ];
	 *
	 * // You can even mix and match syntaxes
	 * public $bentobox_attributes = [
	 *     'body' => [
	 *         'type' => 'wysiwyg',
	 *         'rules' => 'min:3|max:3'
	 *     ],
	 *     'published_at' => 'type:date|index:false|edit:true|panel:Dates|label:Cool Date!|rules:[min:3|max:5]'
	 * ];
	 * ```
	 *
	 * ### Options
	 *
	 * #### Type
	 * Accepts: `string`
	 * Default: `text`
	 *
	 * ###### Basic Inputs
	 * - `checkbox`
	 * - `date`
	 * - `email`
	 * - `number`
	 * - `password`
	 * This input includes a password confirmation attribute
	 * - `select`
	 * Requires an array of `options` to be included along with this
	 * ```php
	 * 'body' => [
	 *     'type' => 'select',
	 *     'options' => [
	 *        'optionA',
	 *        'optionB',
	 *        'optionC'
	 *     ]
	 * ];
	 * ```
	 * - `text`
	 * - `textarea`
	 *
	 * ###### Advanced Inputs
	 * - `wysiwyg`
	 * By default Bentobox uses TinyMCE as it's wysiwyg editor.
	 * - `image`
	 * TODO: Fill this out
	 * - `file`
	 * Simple file upload with some optional configuration. To be used in conjunction with a
	 * string DB attribute where the path will be saved after uploading. If no further settings are
	 * specified, the disk defaults to public, the filepath is to 'files' and the
	 * filename is a timestamp.
	 * ```php
	 * 	'type' => 'file',
	 * 	'disk' => 'public',
	 * 	'filename' => 'example_file',
	 * 	'filepath' => 'example/path/to_file'
	 * ```
	 *
	 * ###### Relationship Inputs
	 * Every relationship input requires you to add in an option called `model` and `attribute` for the
	 * corrisponding related model and the attribute you would like to display from the related model.
	 *
	 * The name of the attribute should also match the method on the model that describes on the model you
	 * are configuring. Eventually we will add the option to add a `relationshipMethod` option to this
	 * so you can have more control.
	 *
	 * - `morphToMany`
	 * ```php
	 * 'partners' => 'type:morphToMany|model:App\Models\Partner|attribute:name'
	 * ```
	 *
	 * #### edit
	 * Accepts: `boolean`
	 * Default: `true`
	 *
	 * Whether or not to show this in the model editor
	 *
	 * ```php
	 * 'title' => 'edit:false'
	 * ```
	 *
	 * #### index
	 * Accepts: `boolean`
	 * Default: `true`
	 *
	 * Whether or not to show this in the index for a model (the page with the table)
	 *
	 * ```php
	 * 'title' => 'index:false'
	 * ```
	 *
	 * #### label
	 * Accepts: `string`
	 * Default: humanized version of the attribute name
	 *
	 * This can be set to any string and that is what will be seen in admin panel when referring
	 * to the attribute
	 *
	 * #### panel
	 * Accepts: `string`
	 * Default: `null`
	 *
	 * This exposes an easy way to organize your attributes into logical groups. You can name the panel
	 * any string and will group up with any other attribute that has that same panel string.
	 * ```php
	 * [
	 *     'background_image' => 'type:image',
		 *
	 *     'partners' => 'type:morphToMany|model:App\Models\Partner|attribute:name|index:false|panel:Relationships',
	 *     'services' => 'type:hasMany|model:App\Models\Service|attribute:name|index:false|panel:Relationships',
	 *     'meta' => 'type:hasOne|model:App\Models\Meta|attribute:title|index:false|panel:Relationships',
		 *
	 *     'original_content_content' => 'type:textarea|index:false|panel:Original Content',
	 *     'original_content_link' => 'index:false|edit:false|panel:Original Content',
	 *     'original_content_cta' => 'index:false|panel:Original Content',
		 *
	 *     'services_block_content' => 'type:textarea|index:false|panel:Services Block',
	 *     'services_block_link' => 'index:false|edit:false|panel:Services Block',
	 *     'services_block_cta' => 'index:false|panel:Services Block',
		 * ];
	 * ```
	 *
	 * @var array
	 */
	public $bentobox_attributes = [];

	/**
	 * Depending on base configuration setting, Bentobox will
	 * include or exclude the modle being passed
	 *
	 * Convenience if not in `$bentobox_config` This will override `$bentobox_config`
	 *
	 * @var boolean
	*/
	public $bentobox_toggle = false;

	/**
	 * Never apply Bentobox to this model
	 *
	 * Convenience if not in `$bentobox_config` This will override `$bentobox_config`
	 *
	 * @var boolean
	*/
	public $bentobox_disabled = false;

	/**
	 * Always apply Bentobox to this model
	 *
	 * Convenience if not in `$bentobox_config` This will override `$bentobox_config`
	 *
	 * @var boolean
	*/
	public $bentobox_enabled = false;
}
