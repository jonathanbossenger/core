<?php

namespace Skyrkt\Bentobox\Listeners;

use Skyrkt\Bentobox\Listeners\BentoboxModelHandler;

class InsertSortValues extends BentoboxModelHandler
{
	/**
	* Fire sequence of tasks required on model if the
	* model has any usort fields in it.
	*
	* @return void
	*/
	protected function fire()
	{
		foreach($this->attributes as $title => $attribute)
		{
			$type = array_get($attribute, 'type');
			if ($type == 'usort') {
				$this->setSortValue($title);
			}
		}
	}

	/**
	 * Check what the highest sorted value is in this particular
	 * attribute and increment it one value higher. Set to 1 if
	 * there are no other records found.
	 *
	 * @param string $attribute
	 * @return void
	 */
	protected function setSortValue($attribute)
	{
		$lastRecord = $this->model::orderBy($attribute, 'desc')->first();

		if ($lastRecord) {
			$this->model->$attribute = $lastRecord->$attribute + 1;

		} else {
			$this->model->$attribute = 1;
		}
	}
}
