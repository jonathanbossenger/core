<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Administration Route Prefix
	|--------------------------------------------------------------------------
	|
	| Bentobox by default will simply use the /admin route for access to the
	| control panel. This can however be configured to any other route
	| that might better work inside your applications structure.
	*/

	// 'route_prefix' => 'admin',

	/*
	|--------------------------------------------------------------------------
	| Model Configuration Mode
	|--------------------------------------------------------------------------
	|
	| Bentobox by default includes all models in your namespaced app directory.
	| Setting to true requires individual models to be included via
	| setting a "public $bentobox_toggle = true;" on the model. While manual
	| inclusion mode is false, setting $bentobox_toggle to true will make
	| Bentobox ignore the model. You can override any of the toggle and inclusion
	| mode settings my setting $bentobox_disabled = true or $bentobox_enabled = true.
	*/

	// 'manual_inclusion_mode' => false,

	/*
	|--------------------------------------------------------------------------
	| Model Directories
	|--------------------------------------------------------------------------
	|
	| Bentobox will automatically include two folders in its search for models
	| `app_path()` and `app_path('Models')`. If you have more directories that
	| Bentobox will need to search for model in, add them here as an array.
	*/

	// 'model_directories' => [
	//   app_path('custom-folder'),
	//   app_path('custom-folder2')
	// ],

	/*
	|--------------------------------------------------------------------------
	| Bentobox Media default image upload transformations
	|--------------------------------------------------------------------------
	|
	| Bentobox by default will create a thumbnail and then orientate (cell phone
	| images, for instance) the originally uploaded image. In addition, you can
	| include additional default transformations to be applied to any
	| BentoboxMedia trait enabled model's image uploads. These will be overridden
	| by any model attribute specific transformations you may specify on your model.
	*/

	// 'transformations' => [
	//   [
	//     'transform_name' => 'default',
	//     'quality' => 70,
	//     'width' => '1920',
	//     'height' => '1080',
	//     'orientate' => true,
	//     'constrainUpsize' => true,
	//     'constrainAspectRatio' => true
	//   ],
	//   [
	//     'transform_name' => 'sm',
	//     'quality' => 80,
	//     'width' => '768',
	//     'height' => '768',
	//     'orientate' => true,
	//     'constrainUpsize' => true,
	//     'constrainAspectRatio' => true
	//   ]
	// ],

	/*
	|--------------------------------------------------------------------------
	| Bentobox Media default disk
	|--------------------------------------------------------------------------
	|
	| Select the disk you'd like Bentobox Media to be saved to. By default we
	| use the 'public' disk that's provided on a default laravel install.
	*/

	// 'bentobox_media_disk' => 'public',

	/*
	|--------------------------------------------------------------------------
	| Bentobox Media default directory path
	|--------------------------------------------------------------------------
	|
	| Select a base directory for all of the Bentobox managed media to be stored
	| in. If none is selected, we'll use the 'bentobox_media' directory.
	*/

	// 'bentobox_media_path' => 'bentobox_media',

	/*
	|--------------------------------------------------------------------------
	| Bentobox Media delete old media on record update
	|--------------------------------------------------------------------------
	|
	| If you would like Bentobox to delete the old media assets when updating
	| a media record, set this to true.
	*/

	// 'destroy_old_media' => false,

	/*
	|--------------------------------------------------------------------------
	| Bentobox Media CDN URL
	|--------------------------------------------------------------------------
	|
	| If you use a CDN you can make the URLS
	| return with this entry as their host.
	*/

	// 'bentobox_media_cdn_url' => 'https://assets.example.com',
];
