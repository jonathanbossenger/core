const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

let webpackConfig = {

  entry: {
    app: path.join(__dirname,'src/frontend/app.js')
  },

  output: {
    path: path.join(__dirname, 'publishable/public'),
    filename: '[name].js'
  },

  devtool: 'source-map',

  module: {
    loaders: [
      {
        enforce: 'pre',
        test: /\.js$/,
        loader: 'eslint-loader',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.css$/,
        loader: ExtractTextPlugin.extract({
          fallbackLoader: 'style-loader',
          loader: [
            'css-loader?sourcemap&importLoaders=1',
            'postcss-loader'
          ]
        })
      },
      {
        test: /\.(jpg|png|gif)$/,
        loader: 'file-loader'
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin('app.css'),
    new StyleLintPlugin({
      files: ['src/frontend/**/*.css'],
      failOnError: false,
    }),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, 'src/frontend/shared/inputs/advanced/wysiwyg/wysiwyg-content.css'),
        to:  path.join(__dirname, 'publishable/public/wysiwyg-content.css')
      },
      {
        from: path.join(__dirname, 'src/frontend/core/styles/iconfonts'),
        to:  path.join(__dirname, 'publishable/public')
      },
      {
        context: path.join(__dirname, '/vendor'),
        from: path.join(__dirname, '/vendor/**/*.min.js'),
        to:  path.join(__dirname, 'publishable/public/vendor')
      },
      {
        context: path.join(__dirname, 'src/frontend'),
        from: path.join(__dirname, 'src/frontend/**/*.blade.php'),
        to:  path.join(__dirname, 'publishable/views')
      }
    ])
  ]
};

if (process.env.NODE_ENV === 'local') {
  webpackConfig.output = {
    path: path.join(__dirname, '../../../public/vendor/bentobox'),
    filename: '[name].js'
  }
  webpackConfig.plugins.push(new CopyWebpackPlugin([
    {
      from: path.join(__dirname, 'src/frontend/core/styles/iconfonts'),
      to:  path.join(__dirname, '../../../public/vendor/bentobox')
    }
  ]))
}

if (process.env.NODE_ENV === 'production') {
  webpackConfig.plugins.push(
    new OptimizeCssAssetsPlugin({
      cssProcessorOptions: {
        discardComments: {
          removeAll: true
        }
      }
    })
  );
}

module.exports = webpackConfig;

