@extends('bentobox::app')

@section('content')
<h1 class="h2">Dashboard</h1>
<p>Welcome to Bentobox, {{ Auth::user()->name }}</p>
<p>
    You are logged in!
</p>
@endsection
