<div class="form-group material @if (array_get($model, $attribute_name, null)) has-value @endif">
		<label for="{{ $attribute_name }}">@bentoLabel($attribute)</label>
		<textarea type="text" @bentoInput($attribute) rows="5">{{ array_get($model, $attribute_name, null) }}</textarea>
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>
