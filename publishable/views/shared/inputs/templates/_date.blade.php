<div class="form-group material has-value">
		<label for="{{ $attribute_name }}">@bentoLabel($attribute)</label>
		<input type="date"
					 name="{{ $attribute_name }}"
					 id="{{ $attribute_name }}"
					 value="{{ array_get($model, $attribute_name) }}"
					 {{ array_get($attribute, "required") ? "required" : "" }}>
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>
