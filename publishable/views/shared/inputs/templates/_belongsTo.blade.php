<?php
    $belongsTo = array_get($attribute, 'model');
    $belongsTo = $belongsTo::all();
    $column = array_get($attribute, 'attribute');
?>
<div class="form-group material has-value">
    <label for="{{ $attribute_name }}">
      @bentoLabel($attribute)
    </label>
    <select name="{{$attribute_name}}" @bentoInput($attribute)>
        @if(!$attribute['required'])
            <option></option>
        @endif
        @foreach($belongsTo as $record)
            <option
              value="{{ $record->id }}"
              @if ($record->id == $model[$attribute_name]) selected @endif>
                {{ $record->$column }}
            </option>

        @endforeach
    </select>
    @include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>

