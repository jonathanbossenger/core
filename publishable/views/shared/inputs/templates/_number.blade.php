<div class="form-group material @if (array_get($model, $attribute_name, null)) has-value @endif">
		<label for="{{$attribute_name}}">@bentoLabel($attribute)</label>
		<input type="number" @bentoInput($attribute)>
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>
