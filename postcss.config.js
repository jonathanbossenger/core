module.exports =  {
  plugins: [
    require('postcss-smart-import')({
      path: [
        'src/frontend/core/styles'
      ]
    }),
    require('postcss-cssnext'),
    require('postcss-responsive-type')()
  ]
};
